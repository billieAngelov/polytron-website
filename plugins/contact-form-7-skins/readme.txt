=== Contact Form 7 Skins ===
Contributors: buzztone
Tags: contact form 7, drag & drop form editor, contact form 7 template, contact form 7 style, contact form 7 theme
Requires at least: 4.3
Tested up to: 5.0
Stable tag: 2.0.1
Author URI: https://cf7skins.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Makes creating Contact Form 7 forms much easier – includes drag & drop Visual Editor together with range of Templates & Styles.

== Description == 

[CF7 Skins](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) works right within the normal Contact Form 7 interface, making it easier for regular WordPress users to create Contact Form 7 forms - even if you don't have HTML + CSS skills.

Includes a [drag & drop Visual Editor](https://cf7skins.com/visual-editor/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) together with range of compatible **Templates** and **Styles**.

Select from a list of compatible [Templates](https://cf7skins.com/templates/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) that cover many common forms and then choose from a range of professional and beautiful [Styles](https://cf7skins.com/styles/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description).

Each **Template** acts as an easy to follow guide, which can be adapted to your requirements. Every **Style** covers the full range of Contact Form 7 form elements.

CF7 Skins is highly customizable and easy to learn, even for beginners.

> CF7 Skins now includes a **[drag & drop Visual Editor](https://cf7skins.com/visual-editor/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)** to help make building your Contact Form 7 forms much easier.

= CF7 Skins Features =

* Use a **[drag & drop Visual Editor](https://cf7skins.com/visual-editor/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)** to create your forms
* Select from a list of ready to use **[Templates](https://cf7skins.com/templates/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)**
* Choose from a range of compatible form **[Styles](https://cf7skins.com/styles/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)**
* Easily create complex forms using Contact Form 7 - **without HTML and CSS knowledge**

> Our [drag & drop Visual Editor](https://cf7skins.com/visual-editor/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) includes support for all Contact Form 7 Tags & options.

[Read more about CF7 Skins](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

[Website](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

= Range of Add-ons available =
Our [CF7 Skins Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) bring the functionality available in other premium WordPress Form plugins directly within Contact Form 7.

[CF7 Skins Pro](https://cf7skins.com/add-ons/pro/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - offers a range of additional Templates and Styles to use with your forms

[CF7 Skins Ready](https://cf7skins.com/add-ons/ready/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - provides a range of useful pre-defined styles that can be used in Contact Form 7 forms

[CF7 Skins Multi](https://cf7skins.com/add-ons/multi/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - includes the features you need to make user friendly multi-part forms

[CF7 Skins Logic](https://cf7skins.com/add-ons/logic/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - allows you to add Conditional Logic to your forms

> All Add-ons include access to [premium email support](https://cf7skins.com/support/email-support/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description).

[Website](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

== Installation ==

1. Visit *Plugins &gt; Add New* in your WordPress admin area and search for Contact Form 7 Skins. 
1. Locate the plugin in search results. 
1. Click on the Install button to download and install the plugin. 
1. Activate the plugin.

*Alternate Installation Method*

1. Download the plugin's zip file and visit *Plugins &gt; Add New* in your WordPress admin area. 
1. Click on the *Upload Plugin* button and then simply upload the zip file. 
1. WordPress will now upload the zip file from your computer to your website, extract it, and install the plugin. 
1. Click on the *Activate* link to start using the plugin.  

Having trouble? Learn more about how to install plugins on [WordPress Codex](http://codex.wordpress.org/Managing_Plugins). 

[Website](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

== Frequently Asked Questions ==

= What does CF7 Skins do? =

CF7 Skins is an add-on plugin for Contact Form 7. It extends the functionality of Contact Form 7 by adding a **drag & drop Visual Editor** together with range of compatible **Templates** and **Styles**. You can learn more by visiting [cf7skins.com](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

= Is there Documentation available? = 

Yes, our in-depth [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) is a great place to find detailed answers. It covers how to get started, how to customize your forms with CF7 Skins and everything else in between.

Many questions have been answered on the [CF7 Skins FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description).

[Website](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

= How do I get started? =

There is a range of [Tutorials](https://kb.cf7skins.com/category/tutorials/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) available to help you get started using CF7 Skins. 

= Where can I get support? =

**Add-ons** - if you have a current license you can post your questions to our [Premium Email Support](https://cf7skins.com/support/email-support/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description).

**Free Version** - you can use the [WordPress Support forum](https://wordpress.org/support/plugin/contact-form-7-skins). This is community based support offered by other CF7 Skin users (we visit the forum intermittently to assist with plugin bugs only).

= What users have to say =
Read [testimonials](https://cf7skins.com/testimonials/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) from CF7 Skins users.

= Do I need to have coding skills to use CF7 Skins? =

Absolutely not. You can create and manage Contact Form 7 forms without any coding knowledge (100% drag & drop form builder).

= What Add-ons are available? =

Our [CF7 Skins Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) bring the functionality available in other premium WordPress Form plugins directly within Contact Form 7.

Using our CF7 Skins Add-ons offers a number of advantages:

* All CF7 Skins Add-ons operate directly within the Contact Form 7 interface – making them easy to use
* Your existing familiarity with using Contact Form 7 – you don’t need to learn another WordPress form plugin
* You can still use the many plugins available that extend Contact Form 7 – both free & premium
* Purchase only the CF7 Skins Add-ons your require – mix and match to suit your needs
* Use of Contact Form 7 in your native language – none of the premium WordPress form plugins have such a wide range of translations available
* Continued access to ongoing development of Contact Form 7 – the most widely used WordPress plugin

> All Add-ons include access to [premium email support](https://cf7skins.com/support/email-support/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) by paid support staff.

[CF7 Skins Pro](https://cf7skins.com/add-ons/pro/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - offers a range of additional Templates and Styles to use with your forms.

[CF7 Skins Ready](https://cf7skins.com/add-ons/ready/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - provides a range of useful pre-defined styles that can be used in all Contact Form 7 forms:

* **singleline** – displays elements on a single line
* **column** – displays enclosed elements in a vertical column
* **grid** – implements a CSS form grid
* **box** – displays enclosed elements in a surrounding box
* **horizontal** – displays checkbox & radio button elements in horizontal line

[CF7 Skins Multi](https://cf7skins.com/add-ons/multi/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - includes the features you need to make user friendly multi-part Contact Form 7 forms:

* **Tab Titles** – with click Navigation
* **Progress Bar** – indicates to users where they are
* **Navigation Buttons** – Previous & Next on each Tab
* **Pagination** – current page / total no.
* **Start & End Tabs** – additional Navigation Buttons
* **Thank You Tab** – extra page displayed only after completed form submission
* **Design matches CF7 Skins Style** – styling of multi-form elements matches selected CF7 Skins Style

[CF7 Skins Logic](https://cf7skins.com/add-ons/logic/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) - allows you to add  Conditional Logic for Contact Form 7 forms:

* **Show or Hide** – fields
* **Criteria** – field, user and post info
* **Value** – equals, not equals, contains or changed
* **Compare** – numeric or text values
* **Multiple logic statements** – if all & if any
* **Multiple fields** with varying logic

> **Conditional Logic** allows you to apply rules to your Contact Form 7 form based on the user’s input and behavior – you can control what information your user is asked to provide and tailor the form specifically to their needs.

[Website](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

= Translations =

* Indonesian (Bahasa Indonesia; id_ID) - Sastra Manurung

[Website](https://cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Documentation](https://kb.cf7skins.com/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [FAQ](https://kb.cf7skins.com/faq/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description) | [Add-ons](https://cf7skins.com/add-ons/?utm_source=wporg&utm_medium=link&utm_campaign=freeversion&utm_content=description)

== Screenshots ==

1. CF7 Skins user interface showing **Form**, **Template** and **Style** tabs
2. CF7 Skins provides a drag & drop **Visual Editor** for your forms 
3. CF7 Skins includes a range of **Templates** to help you get started
4. CF7 Skins comes with many beautiful **Styles** for your finished forms
5. **Drag-and-drop** new fields onto your form
6. **Edit** each field to match your particular requirements
7. Completed **CF7 Skins Form**

== Changelog ==

= 2.0.1 - 2018-08-13 =

* FIX: Prevent JavaScript conflict with Yoast SEO

= 2.0 - 2018-06-19 =

* FEATURE: Add drag & drop Visual Editor
* FEATURE: Display Notices when update plugin
* FIX: Update template files to match visual files
* FIX: Ensure CF7 Skins connects to CF7 Form section
* TWEAK: Show Getting Started Tab when activate plugin
* TWEAK: Ensure deselecting within Template or Style List also deselects in Details or Expanded View

= 1.2.2 - 2018-01-11 =

* FEATURE: Add opt-in data collection
* FIX: Improve context and conjugation issues for non-Roman languages
* FIX: Ensure CF7 Skins scripts are loaded if CF7 default script de-registered
* TWEAK: Hide Info Tabs at WordPress Small screen: 850px
* TWEAK: Ensure active Tab is highlighted
* TWEAK: Remove WP hidden class

= 1.2.1 - 2017-06-07 =

* FIX: WPCF7_Shortcode & WPCF7_ShortcodeManager deprecated in CF7 4.6
* FIX: Ensure key input exists when activating license
* FIX: Remove locale parameter for CF7 >= 4.4
* FIX: Display activation log data
* FIX: Stop default Ready CSS over-writing selected Style CSS
* TWEAK: Add JS trigger for React components
* TWEAK: Add Skins Tabs via filter
* TWEAK: Remove Default CF7 form Template
* TWEAK: Include typicons
* TWEAK: Save version installed when update
* TWEAK: Correct Info Tabs text

= 1.2 - 2016-04-08 =

* FEATURE: Show Template & Style selected in Skin Info Area
* FEATURE: Info Tabs in Skins Meta box & CF7 Skins Settings
* FEATURE: Add Styles
* FIX: Copy CF7 Skins data when CF7 Duplicate used
* FIX: Deselect Template & Style
* TWEAK: Check CF7 User Permissions
* TWEAK: Update colors on Admin messages
* TWEAK: Suitable for wordpress.org translation

= 1.1.2 - 2016-03-10 =

* FIX: Ensure CF7 Skins Styles are enqueued if Contact Form 7 styles are de-registered
* FIX: Ensure update version checking is operating
* FIX: Remove undefined index notice when updating multiple plugins
* FIX: Skins Metabox toggle not sticking

= 1.1.1 - 2015-09-21 =

* FEATURE: Add support for Ready styles
* FIX: Enqueue styles for non-content shortcodes
* TWEAK: Add tabs & consistent spacing to templates
* TWEAK: Improve readability of Logs tab

= 1.1 - 2015-06-30 =

* FEATURE: Add Styles
* FIX: Ensure all plugin data is deleted
* TWEAK: Enqueue only where necessary within Contact Form 7
* TWEAK: Update Help Ballons

= 1.0.2 - 2015-05-29 =

* FIX: wpcf7_add_meta_boxes action removed in Contact Form 7 4.2
* FIX: Parse the CF7 shortcode ID in single or nested shortcodes
* FIX: Ensure JavaScript finds active textarea
* TWEAK: Add cf7skins_form_classes filter hook

= 1.0.1 - 2015-05-06 =

* FIX: Default CSS overriding input & textarea Styles

= 1.0 - 2015-04-09 =

* Initial Release
