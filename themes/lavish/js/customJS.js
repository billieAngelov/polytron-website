// Java Script / jquery


jQuery(function() {

    Mobile.init();
    Benefits.init();
    MobileMenu.init();
    MobileMenuOpened.init();
});

const Mobile = {
    init: () => {
        var window_width = jQuery(window).width();

        if (window_width < 1030) {
            jQuery('#header-logo').attr('src', 'wp-content/themes/lavish/white-logo-mobile.png');
            jQuery('.choose-section .content-wrapper.choose .image-box').toggleClass('mobile');
        } else {
            jQuery('#header-logo').attr('src', 'wp-content/themes/lavish/navbar-logo.png');
        }
    }
}

const MobileMenuOpened = {
    init: () => {
        jQuery('a.toggle_button_lavish_menu').on('click', function() {
            var hasCloseButton = jQuery(this).hasClass("special");
            jQuery(this).toggleClass('special');
            jQuery('.lavish_header').toggleClass('special');
            if (hasCloseButton) {
                jQuery('#header-logo').attr('src', 'wp-content/themes/lavish/white-logo-mobile.png');
            } else {
                jQuery('#header-logo').attr('src', 'wp-content/themes/lavish/black-logo-navbar.png');
            }
        });
    }
}

const Benefits = {
    init: () => {
        jQuery('.benefits-section .read-more-btn').click(Benefits.toggleReadMore);
    },
    toggleReadMore: (e) => {
        const element = e.currentTarget;
        
        jQuery(element).toggleClass('expanded');
        jQuery(element).closest('div').toggleClass('expanded');

        const isOpen = jQuery(element).hasClass('expanded');

        if (isOpen) {
            jQuery(element).text('Close')
        } else {
            jQuery(element).text('Read More');
            Benefits.scrollTo();
        }
    },
    scrollTo: () => {
        var window_width = jQuery(window).width();
        if(window_width > 1030) {
            window.scrollTo(0, 1500);
        } 
    }
}

const MobileMenu = {
    init: () => {
        jQuery('.mobilemenu .menu-item-object-page:nth-child(2)').prepend('<div class="mobile-menu-icon"></div>');       
        jQuery('.mobile-menu-icon').click(function(e) {
            MobileMenu.hideSubMenu(e);
        });
    },
    hideSubMenu: (e) => {   
        const element = e.currentTarget;
        jQuery(element).toggleClass('color');
        jQuery('.sub-menu').toggleClass('show');
    }
}
