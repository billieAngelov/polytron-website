<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package lavish
 * @since 1.0.3
 */
?>
<?php get_sidebar( 'bottom' ); ?>
<!-- Our Pledge moto -->
<section class='our-pledge'>
   <div class='content-wrapper'>
        <div class='pledge-content-box'>
            <img src='wp-content/themes/lavish/wheel-image.png' alt='logo' height='130' width='130'/> 
            
            <div class='pledge-holder'>
                <label class='pledge-label'>Our Pledge</label>
                <div class='pledge-text'>Polytron will significantly extend the life of your equipment and dramatically reduce operational costs and downtime.</div>
            </div>
        </div>
    </div>
</section>
<!-- Our Pledge moto -->


<section class='footer-section'>
    <div class='content-wrapper footer'>
        <div class='footer-logo-holder'>
            <img src='wp-content/themes/lavish/polytron-grey-logo.png' height='35' width='189' alt='logo'/>
        </div>
        <div class='footer-container'>
            <div class='footer-column menu'>
                <a class='footer-link' href='#'>Home</a>
                <a class='footer-link' href='#'>Solutions</a>
                <a class='footer-link' href='#'>MTC Additive</a>
                <a class='footer-link' href='#'>Distributors</a>
                <a class='footer-link' href='#'>Resources</a>
                <a class='footer-link' href='#'>About</a>
                <a class='footer-link' href='#'>Contact</a>
                <a class='footer-link' href='#'>Blog</a>
            </div>
            <div class='footer-column industries'>
                <label class='footer-label'>INDUSTRIES WE SERVE</label>
                <a class='footer-link' href='#'>Automotive</a>
                <a class='footer-link' href='#'>Marine</a>
                <a class='footer-link' href='#'>Rail</a>
                <a class='footer-link' href='#'>Manufacturing</a>
                <a class='footer-link' href='#'>Agricultural</a>
                <a class='footer-link' href='#'>Construction</a>
                <a class='footer-link' href='#'>Mining</a>
                <a class='footer-link' href='#'>Oil and gas</a>
                <a class='footer-link' href='#'>Military</a>
            </div>
            <div class='footer-column products'>
                <label class='footer-label'>OUR PRODUCTS</label>
                <a class='footer-link' href='#'>Polytron Metal Treatment Concentrate (MTC)</a>
                <a class='footer-link' href='#'>Polytron EP-2 Grease</a>
                <a class='footer-link' href='#'>Polytron Penetrating Lubricant</a>
                <a class='footer-link' href='#'>Polytron Ultra Performence Gasoline/Diesel Fuel Conditioner</a>
                <a class='footer-link' href='#'>Polytron Ultra Performance Motor Oil</a>
            </div>
            <div class='footer-column links'>
                <div>
                    <a class='footer-download-catalog' href='#'>Download product catalog</a>
                    <a class='footer-download-guide' href='#'>Download product application guide</a>
                </div>
                <div>
                    <a class='footer-email' href='#'>contact@polytron-lubes.com</a>
                    <a class='footer-number' href='#'>555-555-55555</a>
                </div>
            </div>
        </div>
    </div>
    <div class='footer-policy'>
            <div class='policy-date'>© 2019 Polytron</div>
            <div class='policy-rights'>All rights reserved</div>
            <div class='policy-terms'><a href='#'>Terms of Use</a></div>
            <div class='policy-privacy'><a href='#'>Privacy Policy</a></div>
    </div>
</section>           

<?php wp_footer(); ?>
</body>
</html>