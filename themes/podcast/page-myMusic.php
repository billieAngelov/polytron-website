<?php get_header(); ?>

<!-- /*
    Template Name: My Music Page
*/ -->

<!-- ______________________Custom Fields -->

<?php 

$myMusicHeader_title    = get_post_meta(11, 'myMusicHeader_title', true);
// $whoAmI_text        = get_post_meta(126, 'whoAmI_text', true);
// $whatIDo_text       = get_post_meta(126, 'whatIDo_text', true);
// $whereToFind_text   = get_post_meta(126, 'whereToFind_text', true);

?>


<main id="site-main">

    <!-- <div class="welcomeContainer animated fadeInUp delay-1s"> Welcome To My Music World </div> -->
    <section class='my-music-header'>
            <h1 class="animated fadeInUp delay-1s dance-h1"><?php echo $myMusicHeader_title; ?></h1>
    </section>

    <section class='my-music-section'>
        <div class="animated fadeIn delay-1s">
            <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/519068568&color=%23748186&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        </div>
        <div class="animated fadeIn delay-1s">
            <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/518621955&color=%23748186&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        </div>
        <div class="animated fadeIn delay-1s">
            <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/540617301&color=%23748186&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        </div>
        <div class="animated fadeIn delay-1s">
            <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/514117980&color=%23748186&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
        </div>
        <div>
        <button class="music-button animated fadeIn delay-3s" onclick="typeWriter()">Rights<ion-icon name="arrow-dropdown"></ion-icon></button>
        <p class="white-paragraph" id="selfType"></p>
        </div>
    </section>


    <script>
    var i = 0;
    var txt = 'All rights of the songs are reserved. Join me in the Urban World!';
    var speed = 35;

    function typeWriter() {
        if (i < txt.length) {
            document.getElementById("selfType").innerHTML += txt.charAt(i);
            i++;
            setTimeout(typeWriter, speed);
            }
    }
    </script>

</main><!-- #site-main -->
<?php get_footer('puzzle') ?>
	
