
		    <div id="site-footer-credit">
				<div class="site-section-wrapper site-section-wrapper-footer-credit">
					<p class="site-credit">
					Copyright © 2018 dj puzzle. All Rights Reserved.
						<span class="theme-credit">
							<a href="https://www.facebook.com/bobi.angelov.92" rel="nofollow,noopener" target="_blank" title="My World">Billie_Angelov.com</a>
						</span> 
					</p>
				</div>
				<div class='social-icons'>
					<a target="_blank" rel="noopener noreferrer" href='https://www.facebook.com/bobi.angelov.92' ><ion-icon class='social-icon' name="logo-facebook"></ion-icon></a>
					<a target="_blank" rel="noopener noreferrer" href='https://www.youtube.com/channel/UC10QgGLgThBk0NBJiLQWnpw'><ion-icon class='social-icon' name="logo-youtube"></ion-icon></a>
					<a target="_blank" rel="noopener noreferrer" href='https://soundcloud.com/djpuzzleoriginal'><ion-icon class='social-icon' name="cloudy"></ion-icon></a>
        		</div>
			
    		</div>

	</div><!-- .site-wrapper-all .site-wrapper-boxed -->

</div><!-- #container -->

<?php 
wp_footer(); 
?>

</body>
</html>