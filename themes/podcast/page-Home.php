<?php get_header(); ?>

<!-- /*
    Template Name: Home Page
*/ -->

<!-- ______________________Custom Fields -->


<?php 

$header_title       = get_post_meta(7, 'header_title', true);
$whoAmI_text        = get_post_meta(7, 'whoAmI_text', true);
$whatIDo_text       = get_post_meta(7, 'whatIDo_text', true);
$whereToFind_text   = get_post_meta(7, 'whereToFind_text', true);

?>

<main id="site-main">

<!-- _________________________________________________________________________ -->



  <header class='home-header'>
      <div>
          <h1 class="h1-header animated fadeInUp delay-1s"><?php echo $header_title; ?></h1>
          <h2 class="h2-header-home animated fadeInUp delay-1s">The Urban One</h2>
      </div>
  </header>

    <section class='centered-section'>
        <div class='welcome-container'>
            <h2 class='h2-header'>Who am I?</h2>
                <p class='my-para'><?php echo $whoAmI_text; ?></p>
        </div>
    </section>
    <section class='who-am-i-section'>

    </section>
    <section class='centered-section silver'>
        <div class='welcome-container'>
            <h2 class='h2-header'>What I Do?</h2>
                <p class='my-para'><?php echo $whatIDo_text; ?></p>
        </div>
    </section>
    <section class='what-i-do-section'>

    </section>
    <section class='centered-section'>
        <div class='welcome-container'>
            <h2 class='h2-header'>Where To Find?</h2>
                <p class='my-para'><?php echo $whereToFind_text; ?></p>
        </div>
    </section>

    <div class="site-page-content">
        <div class="site-section-wrapper site-section-wrapper-main clearfix">
            <div class="site-column site-column-content">
                <div class="site-column-wrapper clearfix">
                    <p class="page-title archives-title">
                        <span class="page-title-span">
                            Billie Angelov 
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>

          <!-- ____________________________SLIDER -->

    <div>
        <?php echo do_shortcode('[slick-carousel-slider arrows="true"]'); ?> 
    </div>

  <!--End ____________________________SLIDER -->



  
    <div class='info-container'>
        <button onclick="topFunction()">Top</button>
    </div>

    <script>
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>

</main><!-- #site-main -->
<?php get_footer('puzzle') ?>

	