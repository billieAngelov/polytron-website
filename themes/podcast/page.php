<?php get_header(); ?>

<main id="site-main">

<?php
while (have_posts()) : the_post();
?>

	<section class='contactBackgroundSection'>
		<div class="site-page-content">
			<div class="site-section-wrapper site-section-wrapper-main clearfix">
			
				<div class='contactFormDiv'>
					<?php

					// Function to display the START of the content column markup
					ilovewp_helper_display_page_content_wrapper_start();

						ilovewp_helper_display_content($post);

					// Function to display the END of the content column markup
					ilovewp_helper_display_page_content_wrapper_end();

					?>
				</div>
			</div><!-- .site-section-wrapper .site-section-wrapper-main -->
		</div><!-- .site-page-content -->
	</section>

<?php
endwhile;
?>
 <div class='info-container'>
        <button onclick="topFunction()">Top</button>
    </div>

    <script>
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>

</main><!-- #site-main -->
	
<?php get_footer("puzzle"); ?>
