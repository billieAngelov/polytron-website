<?php get_header(); ?>

<!-- /*
    Template Name: My Dancing Page
*/ -->

<!-- ______________________Custom Fields -->

<?php 
$myDancingHeader_title   = get_post_meta(9, 'myDancingHeader_title', true);
$teacher_text   = get_post_meta(9, 'teacher_text', true);
$dreamer_text   = get_post_meta(9, 'dreamer_text', true);


?>

<main id="site-main" class="myDancingMain">

    
    <section class='my-dancing-header'>
            <h1 class="animated fadeInUp delay-1s dance-h1"><?php echo $myDancingHeader_title; ?></h1>
    </section>

    <section class='my-dancing-section1'>
        <h2 class='my-dancing-h2'>
            Kizomba Fusion - NDK / 2018
            <p class='my-dancing-p'>Song by: DJ Zayx </p>
        </h2>
        <div class="animated bounceInUp delay-1s video-container">
        <iframe class="youTubeIframe" width="560" height="315" src="https://www.youtube.com/embed/cTvv0B4gbys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>
    <section class='my-dancing-section2'>
        <h2 class='my-dancing-h2'>
            Kizomba Fusion - Pliska / 2017
            <p class='my-dancing-p'>Song by: Michael Jackson</p>
        </h2>
        <div class="animated bounceInUp delay-1s video-container">
        <iframe class="youTubeIframe" width="560" height="315" src="https://www.youtube.com/embed/Tv8RjXWFYBI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>
    <section class='my-dancing-section3'>
        <h2 class='my-dancing-h2'>
            Kizomba Fusion - Kife / 2018
            <p class='my-dancing-p'>Song by: BADMAN</p>
        </h2>
        <div class="animated bounceInUp delay-1s video-container">
        <iframe class="youTubeIframe" width="560" height="315" src="https://www.youtube.com/embed/aPIcsmP-sjU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>

    <section class='centered-section silver'>
        <div class="welcome-container">
            <h3 class='animated lightSpeedIn delay-3s'>Being a Teacher...</h3>
            <p class='animated lightSpeedIn delay-3s my-para'><?php echo $teacher_text; ?></p>
        </div>
    </section>
    <!-- <section class='centered-section blue'>
        <div class="welcome-container">
            <h3 class='animated lightSpeedIn delay-3s'>Being an Inspiration...</h3>
            <p class='animated lightSpeedIn delay-3s my-para'>Something that people find difficult to understand completely is that Kizomba and Real life are very similar. The more you understand yourself in the dance, the more you learn about yourself in the outside world. It's fascinating. That's why I love to inspire people. Nothing better than providing your students with this warm energy. </p>
        </div>
    </section> -->
    <section class='inspiration-section'>

    </section>
    <section class='centered-section'>
        <div class="welcome-container">
            <h3 class='animated lightSpeedIn delay-3s'>Being a Dreamer...</h3>
            <p class='animated lightSpeedIn delay-3s my-para'><?php echo $dreamer_text; ?></p>
        </div>
    </section>

    <div class='info-container'>
        <button onclick="topFunction()">Top</button>
    </div>

       <script>
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>

</main><!-- #site-main -->
<?php get_footer('puzzle') ?>
	
