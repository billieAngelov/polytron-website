<?php get_header(); ?>

<!-- /*
    Template Name: CONTACT Page
*/ -->

<main id="site-main">
	<section class='contact-header'>
			<ion-icon class="animated fadeInUp dance-h1" name="paper-plane"></ion-icon>
			<h1 class="animated fadeInUp dance-h1">Feedback Place</h1>
	</section>

<?php
while (have_posts()) : the_post();
?>

	<section class='contact-background-section'>
		<div class="site-page-content">
			<div class="site-section-wrapper site-section-wrapper-main clearfix">
			
				<div class='contactFormDiv'>
					<?php

					// Function to display the START of the content column markup
					ilovewp_helper_display_page_content_wrapper_start();

						ilovewp_helper_display_content($post);
						ilovewp_helper_display_comments($post);

					// Function to display the END of the content column markup
					ilovewp_helper_display_page_content_wrapper_end();

					?>
				</div>
			</div><!-- .site-section-wrapper .site-section-wrapper-main -->
		</div><!-- .site-page-content -->
		
			<div class='animated fadeIn delay-1s contact-container'>
				<ion-icon class='contact-icon' name="logo-facebook"></ion-icon></a>
                <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/bobi.angelov.92" class='contact-p'>https://www.facebook.com/bobi.angelov.92</a>
            </div>
            <div class='animated fadeIn delay-2s contact-container'>
                <ion-icon class='contact-icon' name="chatbubbles"></ion-icon>
                <a target="_blank" rel="noopener noreferrer" href="mailto:billie.angelov@gmail.com?Subject=Hello%20Puzzle" target="_top" class='contact-p'>billie.angelov@gmail.com</a>
            </div>
            <div class='animated fadeIn delay-3s contact-container'>
                <ion-icon class='contact-icon' name="cloud-outline"></ion-icon>
                <a target="_blank" rel="noopener noreferrer" href="https://soundcloud.com/djpuzzleoriginal" class='contact-p'>https://soundcloud.com/djpuzzleoriginal</a>
            </div>
	</section>

<?php
endwhile;
?>

</main><!-- #site-main -->

<?php get_footer('puzzle'); ?>


	